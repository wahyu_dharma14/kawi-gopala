﻿using KawiGopala.Site.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using umbraco.NodeFactory;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using System.Net.Mail;
using System.Text;

namespace KawiGopala.Site.Controllers
{
    public class ContactController : SurfaceController
    {
        [HttpPost]
        public ActionResult SendEmail(ContactModel model)
        {
            //Check if the dat posted is valid (All required's & email set in email field)
            if (!ModelState.IsValid)
            {
                //Not valid - so lets return the user back to the view with the data they entered still prepopulated
                return Redirect(Request.UrlReferrer.AbsolutePath);
            }

            var root = Umbraco.TypedContentAtRoot().First();

            var contact = root.GetPropertyValue("emailContact").ToString();
            
            //Generate an email message object to send
            MailMessage email = new MailMessage(model.EmailAddress, contact);
            email.Subject = "Contact Form Request";
            email.Body = PopulateContactMailBody(model);

            try
            {
                //Connect to SMTP credentials set in web.config
                SmtpClient smtp = new SmtpClient();

                //Try & send the email with the SMTP settings
                smtp.Send(email);
            }
            catch (Exception ex)
            {
                //Throw an exception if there is a problem sending the email
                throw ex;
            }

            string scsQS = !string.IsNullOrEmpty(Request.UrlReferrer.Query) ? "&scs=1" : "?scs=1";
            return Redirect(Request.UrlReferrer.AbsoluteUri + scsQS);
        }

        private string PopulateContactMailBody(ContactModel model)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Customer Name: " + model.FirstName);
            sb.AppendLine("Customer Email: " + model.EmailAddress);
            sb.AppendLine("Customer Phone: " + model.PhoneNumber);
            sb.AppendLine("Messeage: " + model.Message);
            return sb.ToString();
        }
    }
}