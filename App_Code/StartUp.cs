﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Umbraco.Core;

namespace KawiGopala.Site.App_Code
{
    public class StartUp : IApplicationEventHandler
    {
        public void OnApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            RouteTable.Routes.MapRoute(
               "",
               "Contact/SendEmail",
               new
               {
                   controller = "Contact",
                   action = "SendEmail",
                   id = 0
               });

            RouteTable.Routes.MapRoute(
                "",
                "Product/ProductDetail",
                new
                {
                    controller = "Product",
                    action = "ProductDetail",
                    id = 0
                });
        }

        public void OnApplicationInitialized(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        { }
        public void OnApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        { }
    }
}