﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KawiGopala.Site.Models
{
    public class ProductVIewModel
    {
        public string tag { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string imgUrl { get; set; }
    }
}